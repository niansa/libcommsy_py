import libcommsy_lowlevel
import requests



def curlreq(URL:str, SID:str):
    connection = requests.get(URL, headers={"Cookie": "SID=" + SID})
    return connection.text, connection.status_code


class commsyFile:
    def __init__(self, name:str, url:str):
        self.name = name
        self.url = url


class commsyPost:
    def __init__(self, name:str, id:str, description:str, meta:str, url:str, files:list, unread:bool, taskState:int):
        self.name = name
        self.id = id
        self.description = description
        self.meta = meta
        self.url = url
        self.files = files
        self.unread = unread
        self.taskState = taskState


class libCommsy:
    def __init__(self, server_url:str, server_sid:str, room:str, start_id:str = "", max_posts:int = 0):
        self.posts, self.lastID = libcommsy_lowlevel.fetchAll(server_url, server_sid, room, start_id, max_posts)
        self._server_url = server_url
        self._server_sid = server_sid

    def getDescription(self, postindex:int):
        return libcommsy_lowlevel.fetchDescription(self._server_url + self.posts[postindex].url, self._server_sid)
