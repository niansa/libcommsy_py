# libcommsy_py

A Python wrapper to the scrapping library libCommsy

## Installation
1. Clone repo: `git clone https://gitlab.com/niansa/libcommsy_py.git --recursive`
2. Change to cloned directory: `cd libcommsy_py`
3. Install package requirements `sudo pip3 install -r requirements.txt`
4. Install package: `sudo pip3 install .`

## Example usage
```
from libcommsy import libCommsy

commsy = libCommsy(URL, SID, ROOM)
print("Latest post: " + commsy.posts[0].name + '\n')
print(commsy.getDescription(0))
```
which would be identical to:
```
#include <iostream>
#include "libcommsy.hpp"

int main() {
    auto commsy = libCommsy(URL, SID, ROOM)
    std::cout << "Latest post: " << commsy.getPost(0)->name << std::endl << std::endl;
    std::cout << *(commsy.getDescription(0)) << std::endl;
}
```
