#include <string>
#include <vector>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "libcommsy.hpp"
extern std::string get_post_desc(const std::string& post_url, const std::string& server_sid);

namespace py = pybind11;



auto libCommsyWrap(const std::string& _server_url, const std::string& _server_sid, const std::string& _room, const std::string start_id, const unsigned long max_posts) {
    auto connection = libCommsy(_server_url, _server_sid, _room, start_id, max_posts);

    // Convert result to python classes
    py::object pyBaseCommsyPost = py::module::import("libcommsy_highlevel").attr("commsyPost");
    py::object pyBaseCommsyFile = py::module::import("libcommsy_highlevel").attr("commsyFile");
    std::vector<py::object> pyPosts;
    for (auto& cppPost : connection.posts) {
        std::vector<py::object> files;

        // Get file array
        for (const auto& cppFile : cppPost.files) {
            files.push_back(pyBaseCommsyFile(cppFile.name, cppFile.url));
        }

        // Get final python post class
        if (cppPost.description == "\xFF") {
            cppPost.description = "";
        }

        pyPosts.push_back(pyBaseCommsyPost(cppPost.name, cppPost.id, cppPost.description, cppPost.meta, cppPost.url, files, cppPost.unread, static_cast<int>(cppPost.taskState)));
    }

    return py::make_tuple(pyPosts, connection.lastID);
}

PYBIND11_MODULE(libcommsy_lowlevel, m) {
    m.doc() = "A library to fetch data from commsy servers (lowlevel side)";

    m.def("fetchAll", &libCommsyWrap, "Fetch data from commsy",
          py::arg("server_url"), py::arg("server_sid"), py::arg("room"), py::arg("start_id") = "", py::arg("max_posts") = 0);
    m.def("fetchDescription", &get_post_desc, "Fetch description of commsy post",
          py::arg("post_url"), py::arg("server_sid"));
}
