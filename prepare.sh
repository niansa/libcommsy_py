#! /bin/sh


cd gumbo-parser &&
./autogen.sh &&
CC="gcc -fPIC" ./configure &&
exec make -j
