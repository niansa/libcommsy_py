#include <sstream>
#include <pybind11/pybind11.h>


long curlreq(std::stringstream &responsebuffer, std::string SID, std::string URL) {
    namespace py = pybind11;
    py::object curlreq_py = py::module::import("libcommsy_highlevel");
    py::tuple connection = curlreq_py.attr("curlreq")(URL, SID);
    responsebuffer << connection[0].cast<std::string>();
    return connection[1].cast<long>();
}
