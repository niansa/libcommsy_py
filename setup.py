import setuptools
import shutil
import os, sys


def do_setup():

    ret = os.system("./prepare.sh")
    if ret != 0:
        return ret
    ret = os.system("./compile.sh ./gumbo-parser/.libs/libgumbo.a")
    if ret != 0:
        return ret

    for file in os.listdir():
        copy = False
        if file.endswith(".so"):
            copy = True
        elif file in ["libcommsy_highlevel.py", "__init__.py"]:
            copy = True
        if copy:
            shutil.copy(file, "libcommsy/"+file)

    setuptools.setup(
        name="libcommsy",
        version="1.0",
        author="niansa",
#        author_email="author@example.com",
        description="A Python wrapper to the scrapping library libCommsy",
#        long_description="This is a very very very very very very very long description",
#        long_description_content_type="text/plain",
         url="https://gitlab.com/niansa/libcommsy_py",
         packages=setuptools.find_packages(),
         package_data={'': os.listdir("libcommsy/")},
         classifiers=[
          "Programming Language :: Python :: 3",
#          "License :: OSI Approved :: MIT License",
          "Operating System :: Linux",
        ],
        python_requires='>=3.6',
    )

sys.exit(do_setup())
