#! /bin/bash

if [ "$1" = "" ]; then
    echo "Usage: $0 <path to libgumbo.a>"
    exit 1
fi

exec g++ -O3 -Wall -shared -std=c++17 -fPIC `python3 -m pybind11 --includes` -Iqcommsy libcommsy_lowlevel.cpp curlreq.cpp qcommsy/libcommsy.cpp "$1" -o libcommsy_lowlevel`python3-config --extension-suffix`
